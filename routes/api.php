<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
Route::get('/pacientes',[\App\Http\Controllers\API\PacienteController::class,'index']);
Route::post('/pacientes', [\App\Http\Controllers\API\PacienteController::class, 'store']);
Route::get('/pacientes/{paciente}', [\App\Http\Controllers\API\PacienteController::class, 'show']);
Route::put('/pacientes/{paciente}', [\App\Http\Controllers\API\PacienteController::class, 'update']);
Route::delete('/pacientes/{paciente}', [\App\Http\Controllers\API\PacienteController::class, 'destroy']);
*/


Route::post('/registro',[\App\Http\Controllers\AutenticarController::class, 'registro']);
Route::post('/acceso',[\App\Http\Controllers\AutenticarController::class, 'acceso']);

/**
 * Uso del middleware para q solo el usuairo
 * registrado con un token valido pueda acceder
 * a una ruta determinada.
 */
// Route::middleware('auth:sanctum')->post('/cerrarsesion',[\App\Http\Controllers\AutenticarController::class, 'cerrarSesion']);

/**
 * Para agrupar varias rutas
 * que usen un mismo middleare
*/
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/cerrarsesion',[\App\Http\Controllers\AutenticarController::class, 'cerrarSesion']);
    Route::apiResource('/pacientes',\App\Http\Controllers\API\PacienteController::class);
});
